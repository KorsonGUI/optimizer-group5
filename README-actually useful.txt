  ______ _ _          ____        _   _           _              
 |  ____| (_)        / __ \      | | (_)         (_)             
 | |__  | |_ _ __   | |  | |_ __ | |_ _ _ __ ___  _ _______ _ __ 
 |  __| | | | '_ \  | |  | | '_ \| __| | '_ ` _ \| |_  / _ \ '__|
 | |    | | | |_) | | |__| | |_) | |_| | | | | | | |/ /  __/ |   
 |_|    |_|_| .__/   \____/| .__/ \__|_|_| |_| |_|_/___\___|_|   
            | |            | |                                   
            |_|            |_|                                   

  ___ ___   _   ___  __  __ ___ 
 | _ \ __| /_\ |   \|  \/  | __|
 |   / _| / _ \| |) | |\/| | _| 
 |_|_\___/_/ \_\___/|_|  |_|___|
                                

The FlipOptimizer by Group5 is far from user friendly.
If you are not ready to dig around in my shitty code and write extremly long and comlicated filenames, flee now, for you have been warned.


To use this Optimizer simply import it as a module into your project and press alt+enter on every error you find, and pray, that intelliJ fixes it. Those are mostly javaFX module path thingys. However your work begins in the ConcurrentRunner class in the method getScorings. There you will have to import your gameInfo implementation and change your simulate method to return your worldinfo implementation, not WorldInfo. Furthermore your world needs to Implement a method used later, called getSwarms and that Method needs to return a Map<Character, Swarm> and your Swarm class needs to have its currentScore and it needs to have a getter for that score.

If all of that is the case, or you fixed the scoring system some other way, which is more fitting for your implementation... Hurray, you can now start naming and adjusting your brains to fit my lazy parsing.

Create a folder and put all maps into it. The Maps need to be named accordin to this pattern, where anything in '<>' need to be replaced, along with the brackets:
<Number of different bases on that map>_<name of the map>
None of these components may be left out.

Your brains need to be changed as well. Inside the acola code, any flip value you want to optimize must be written according to this pattern:

$[<Name of the variable>]
REGEX to match: "[$][[].*[\]]"

You may use this variable as many times as you wish, and you can use as many different variables as you wish, but I recommend to only optimize for flips that are acutally important (e.g. when to breed and when to run home) to reduce run time.

After you added the variables, name the file according to this pattern:

<number of variables to optimize>_<name of a variable>_<name of a variable>...._<name of the brain>




You did all of this? Great, now use the optimizer. Start the class OptimizerGUI and chooso the folder with the maps, and the brain to optimize.
Then choose the lowest number of rounds to optimize for and the highest amount.
Then choose a number of weights to test. The higher this number, the higher the probability, that you'll find an optimized weight but also increases the runtime.
Then choose the maximum that is added for the flip. I recommend 10 or something around that, so you don't create to many shitty weights.
then choose the amount of steps the optimizer shall take between the min and max.
Press the start optimization button, and watch the command line of your IDE since any progress is shown there.

DISCLAIMER: THIS TAKES A LONG TIME, EVEN WITH CONCURRENT THREADS. WAIT AND WATCH THE COMMAND LINE AND DO NOT ENTER STUPID VALUES OR YOUR PC WILL CRASH.

