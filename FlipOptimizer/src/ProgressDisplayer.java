import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

public class ProgressDisplayer extends Application {


    public static final CountDownLatch latch = new CountDownLatch(1);
    public static ProgressDisplayer progressDisplayer = null;
    private Stage primaryStage;
    private AnchorPane anchorPane;
    private ProgressDisplayer me;



    public BarChart getBarChart() {
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String, Number> bc =
                new BarChart<>(xAxis, yAxis);
        bc.setTitle("Current Scores");
        xAxis.setLabel("Weight");
        yAxis.setLabel("Score");

        XYChart.Series series = new XYChart.Series();
        series.setName("Score over all maps");
        for (WeightScore weightScore : getParsedData()) {
            series.getData().add(new XYChart.Data(getDisplayString(weightScore.getWeights()), weightScore.getScore()));
        }
        bc.getData().add(series);
        return bc;
    }

    private List<WeightScore> getParsedData() {
        List<WeightScore> res = new ArrayList<>();
        try {
            String raw = new String(Files.readAllBytes(new File("./resources/data.txt").toPath()), StandardCharsets.UTF_8);
            String[] split = raw.split("[\n]");
            for (String line : split) {
                String[] data = line.split("[:]");
                res.add(new WeightScore(getVector(data[0]), Integer.parseInt(data[0])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    private Vector<Integer> getVector(String datum) {
        String[] vals = datum.split("[,]");
        Vector<Integer> res = new Vector<>(vals.length);
        for (String val : vals)
            res.add(Integer.parseInt(val));
        return res;
    }

    private String getDisplayString(Vector<Integer> weights) {
        StringBuilder str = new StringBuilder().append('[');
        for (Iterator<Integer> iter = weights.iterator(); iter.hasNext(); ) {
            str.append(iter.next() + (iter.hasNext() ? ", " : ""));
        }
        return str.append(']').toString();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        AnchorPane root = new AnchorPane();
        this.anchorPane = root;
        root.getChildren().add(getBarChart());
        root.setPrefSize(720, 720);
        primaryStage.setScene(new Scene(root));
        this.primaryStage = primaryStage;
        primaryStage.show();
    }


}
