import java.util.List;
import java.util.Vector;

public class WeightScore implements Comparable<WeightScore>{

    private int score;
    private Vector<Integer> weights;


    public WeightScore(Vector<Integer> weights, int score){
        this.weights = weights;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Vector<Integer> getWeights() {
        return weights;
    }

    public void setWeights(Vector<Integer> weights) {
        this.weights = weights;
    }


    public void adapt(List<Integer> scores) {
        for(Integer score : scores){
            this.score += score;
        }
        this.score = score / scores.size();
    }

    @Override
    public int compareTo(WeightScore o) {
        return o.getScore() - this.getScore();
    }

    @Override
    public String toString(){
        return "" + weights.toString() + " -> " + score;
    }
}
