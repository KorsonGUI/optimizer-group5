import saarland.cispa.sopra.controllers.Game;
import saarland.cispa.sopra.models.Swarm;
import saarland.cispa.sopra.models.World;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.Callable;

public class ConcurrentRunner implements Callable {

    private String rawBrain;
    private Vector<Integer> weights;
    private File[] maps;
    private WRandom wRandom;
    private int roundsMin;
    private int roundsMax;
    private int stepAmount;
    private String SPACING = "      >";
    private Long ID = null;

    public ConcurrentRunner(String rawBrain, Vector<Integer> weights, File[] maps, WRandom wRandom, int roundsMin, int roundsMax, int stepAmount) {
        this.rawBrain = rawBrain;
        this.weights = weights;
        this.maps = maps;
        this.wRandom = wRandom;
        this.roundsMin = roundsMin;
        this.roundsMax = roundsMax;
        this.stepAmount = stepAmount;
    }

    public Vector<Integer> getWeights() {
        return weights;
    }

    @Override
    public WeightScore call() {
        return runAllMaps(rawBrain, weights);
    }


    private WeightScore runAllMaps(String brain, Vector<Integer> weighting) {
        WeightScore res = new WeightScore(weighting, 0);
        int max = maps.length;
        int j = 1;
        for (File map : maps) {
//            System.out.print("  >Currently working on " + map.getName() + "\n");
            int swarmAmount = getSwarmAmount(map);
            String[] brains = new String[swarmAmount];
            for (int i = 0; i < swarmAmount; i++) {
                brains[i] = brain;
            }
            res.adapt(getScoring(this.roundsMin, this.roundsMax, wRandom.roll(new Random().nextInt()), map, brains));
            if (null == ID)
                ID = getIDDis();
            System.out.print(SPACING + "THREAD " + ID + " finished (" + j + "/" + max + "):" + map.getName() + "\n");
            j++;
        }
        return res;
    }

    private Long getIDDis() {
        return Thread.currentThread().getId();
    }


    private List<Integer> getScoring(int roundsMin, int roundsMax, int roll, File map, String[] brains) {
        List<Integer> scores = new ArrayList<>(10);

        Game game = new Game();
        int stepSize = (roundsMax - roundsMin) / stepAmount;

        int[] steps = new int[stepAmount + 1];
        for (int i = 0; i < stepAmount + 1; i++) {
            steps[i] = roundsMin + i * stepSize;
        }
        for (int step : steps) {
            try {
//                System.out.print("          >Simulating " + step + " on " + map.getName() + "\n");
                scores.add(lamdaScore(game.simulate(step, roll, new String(Files.readAllBytes(map.toPath()), StandardCharsets.UTF_8), brains)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return scores;
    }

    private Integer lamdaScore(World world) {
        Integer res = 0;
        for (Map.Entry<Character, Swarm> swarmEntry : world.getSwarms().entrySet()) {
            res += swarmEntry.getValue().getScore() / world.getSwarms().size();
            res += world.getLivingAnts(swarmEntry.getValue()) / 10;
        }
        return res;
    }

    private int getSwarmAmount(File map) {
        int i = 2;
        try {
            i = Integer.parseInt(map.getName().substring(0, 1));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return i;
    }


}
