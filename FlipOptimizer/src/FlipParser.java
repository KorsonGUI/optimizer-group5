import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Vector;

public class FlipParser {

    private final File brain;
    private Vector<Integer> flipVector;
    private int flipAmount = 5;
    private String rawBrain;
    private Vector<String> flipnames;

    public FlipParser(File brain) {
        this.brain = brain;
    }

    public Vector<Integer> getFlipVector() {
        return this.flipVector;
    }

    public void setFlipVector(Vector<Integer> newVector) {
        this.flipVector = newVector;
    }

    public void parse() throws IOException {
        this.flipAmount = Integer.parseInt(brain.getName().substring(0, 1));
        this.rawBrain = new String(Files.readAllBytes(brain.toPath()), StandardCharsets.UTF_8);
        fillFlipMap();
    }

    private void fillFlipMap() {
        this.flipnames = new Vector<>(flipAmount);
        String[] nameSplit = brain.getName().split("[_]");
        for (int i = 1; i < nameSplit.length - 1; i++) {
            flipnames.add(nameSplit[i]);
        }
    }

    public String write() {
        String rewrite = rawBrain;
        for (int i = 0; i < flipAmount; i++) {
            rewrite = rewrite.replaceAll("[$][\\[]"+ flipnames.get(i)+"[\\]]",""+flipVector.get(i));
        }
        return rewrite;
    }


    public int getFlipAmount() {
        return this.flipAmount;
    }
}
