import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.util.List;

public class OptimizerGUI extends Application {

    private String buttonStyleSheet = "-fx-text-fill: white;" + "-fx-font-family: Arial;" + "-fx-background-color: rgb(16,66,128);" + "-fx-border-color: black";


    private VBox mainBox;
    private File[] maps = null;
    private File brain = null;
    private int roundsMin = -1;
    private int roundsMax = -1;
    private int vectorAmount = -1;
    private int flipMax = -1;
    private int stepAmount = -1;
    private Stage primaryStage;
    private TextField minInput;
    private TextField maxInput;
    private TextField vectorInput;
    private TextField maxFlipInput;
    private TextField stepsizeInput;


    public static void main(String[] args) {
        Application.launch(OptimizerGUI.class);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        AnchorPane root = new AnchorPane();
        root.getChildren().add(getChooserBox());
        root.setPrefSize(250, 250);
        primaryStage.setScene(new Scene(root));

        this.primaryStage = primaryStage;
        primaryStage.show();
    }

    private Node getChooserBox() {
        VBox vb = new VBox();
        this.mainBox = vb;
        vb.setAlignment(Pos.CENTER);
        vb.setPrefSize(250, 250);
        vb.getChildren().add(getMapFolderChooser());
        vb.getChildren().add(getBrainChooser());
        vb.getChildren().add(getMinChooser());
        vb.getChildren().add(getMaxChooser());
        vb.getChildren().add(vectorAmountChooser());
        vb.getChildren().add(maxFlipChooser());
        vb.getChildren().add(stepAmountChooser());
        vb.getChildren().add(getStartButton());
        return vb;
    }

    private Node stepAmountChooser() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        Label stepsizeLabel = new Label("step size:");
        TextField stepsizeInput = new TextField();
        stepsizeInput.setPrefSize(150, 30);
        this.stepsizeInput = stepsizeInput;
        hb.getChildren().add(stepsizeLabel);
        hb.getChildren().add(stepsizeInput);
        return hb;
    }


    private Node getStartButton() {
        Button btn = new Button();
        btn.setText("Start Optimization");
        btn.setOnMouseClicked((MouseEvent e) -> {
            try {
                this.vectorAmount = Integer.parseInt(vectorInput.getText());
                this.roundsMin = Integer.parseInt(minInput.getText());
                this.roundsMax = Integer.parseInt(maxInput.getText());
                this.flipMax = Integer.parseInt(maxFlipInput.getText());
                this.stepAmount = Integer.parseInt(stepsizeInput.getText());
            } catch (Exception e1) {
                throwExceptionPopup(e1);
            }
            if (maps != null && brain != null && vectorAmount != -1 && roundsMin != -1 && roundsMax != -1 && flipMax != -1 && stepAmount != 1) {
                runThis();
            }
        });
        btn.setStyle(buttonStyleSheet);
        btn.setPrefSize(200, 30);
        return btn;
    }

    private Node maxFlipChooser() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        Label maxFlipLabel = new Label("maxFlip:");
        TextField maxFlipInput = new TextField();
        maxFlipInput.setPrefSize(150, 30);
        this.maxFlipInput = maxFlipInput;
        hb.getChildren().add(maxFlipLabel);
        hb.getChildren().add(maxFlipInput);
        return hb;
    }

    private Node vectorAmountChooser() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        Label vectorLabel = new Label("weights:");
        TextField vectorInput = new TextField();
        vectorInput.setPrefSize(150, 30);
        this.vectorInput = vectorInput;
        hb.getChildren().add(vectorLabel);
        hb.getChildren().add(vectorInput);
        return hb;
    }

    private Node getMinChooser() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        Label minRound = new Label("lowest round:");
        TextField minInput = new TextField();
        minInput.setPrefSize(125, 30);
        this.minInput = minInput;
        hb.getChildren().add(minRound);
        hb.getChildren().add(minInput);
        return hb;
    }

    private Node getMaxChooser() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        Label maxRound = new Label("highest round:");
        TextField maxInput = new TextField();
        maxInput.setPrefSize(120, 30);
        this.maxInput = maxInput;
        hb.getChildren().add(maxRound);
        hb.getChildren().add(maxInput);
        return hb;
    }

    private Node getBrainChooser() {
        Button btn = new Button();
        btn.setText("Choose Brain");
        btn.setOnMouseClicked((MouseEvent e) -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("txt", "*.txt"));
            fileChooser.setTitle("Choose brain file to optimize");
            this.brain = fileChooser.showOpenDialog(primaryStage);
        });
        btn.setStyle(buttonStyleSheet);
        btn.setPrefSize(200, 30);
        return btn;
    }

    private Node getMapFolderChooser() {
        Button btn = new Button();
        btn.setText("Choose Trainingmaps");
        btn.setOnMouseClicked((MouseEvent e) -> {
            DirectoryChooser fileChooser = new DirectoryChooser();
            fileChooser.setTitle("Choose a folder containing all training Maps");
            this.maps = fileChooser.showDialog(primaryStage).listFiles();
        });
        btn.setStyle(buttonStyleSheet);
        btn.setPrefSize(200, 30);
        return btn;
    }


    private void runThis() {
        BrainOptimizer bo = new BrainOptimizer(brain, maps, vectorAmount, roundsMin, roundsMax, mainBox, flipMax, stepAmount);
        bo.optimize();
        displayWinners(bo.getScoring());
    }

    private void displayWinners(List<WeightScore> scoring) {
        this.mainBox.getChildren().clear();
        for (int i = 0; i < 10; i++) {
            mainBox.getChildren().add(getDisplayLabel(i, scoring.get(i)));
        }
    }

    private Node getDisplayLabel(int i, WeightScore weightScore) {
        System.out.print("\n----------------DONE----------------");
        HBox hb = new HBox();
        Label label = new Label();
        label.setText(new StringBuilder().append(i).append(".: ").append(weightScore.toString()).toString());
        hb.getChildren().add(label);
        return hb;
    }

    public void throwExceptionPopup(Exception e) {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox dialogVbox = new VBox(20);
        TextArea ta = new TextArea(new StringBuilder().append(e.toString()).append("\n\n\nStacktrace:\n").append(e.getStackTrace()).toString());
        ta.setPrefRowCount(20);
        dialogVbox.getChildren().add(ta);
        Scene dialogScene = new Scene(dialogVbox, 600, 300);
        dialog.setScene(dialogScene);
        dialog.show();

    }


}
