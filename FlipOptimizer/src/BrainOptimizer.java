import javafx.scene.layout.VBox;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class BrainOptimizer {

    private final ProgressDisplayer progressDisplayer = new ProgressDisplayer();
    private File brainFile;
    private int randoms;
    private WRandom wRandom;
    private Vector<Integer>[] vectors;
    private File[] maps;
    private List<WeightScore> scoring = new LinkedList<>();
    private int roundsMin;
    private int roundsMax;
    private VBox vb;
    private int flipMax;
    private int stepAmount;
    private List<FutureTask<WeightScore>> runners = new ArrayList();
    private List<Thread> runnerThreads = new ArrayList<>();

    public BrainOptimizer(File brain, File[] maps, int randoms, int roundsMin, int roundsMax, VBox vb, int flipMax, int stepAmount) {
        this.brainFile = brain;
        this.randoms = randoms;
        this.roundsMin = roundsMin;
        this.roundsMax = roundsMax;
        this.wRandom = new WRandom(42);
        this.maps = maps;
        this.vb = vb;
        this.flipMax = flipMax;
        this.stepAmount = stepAmount;
    }

    public List<WeightScore> getScoring() {
        return scoring;
    }

    public void optimize() {
        FlipParser fp = new FlipParser(this.brainFile);
        try {
            fp.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int length = fp.getFlipAmount();
        Vector<Integer>[] vectors = new Vector[randoms];
        for (int i = 0; i < randoms; i++) {
            vectors[i] = getRandomVector(length);
        }
        this.vectors = vectors;
        train(fp);
    }

    private void train(FlipParser fp) {
        int j = 1;
        int max = vectors.length;
        List<FutureTask<WeightScore>> current = new ArrayList<>();
        for (int v = 0; v < vectors.length; v++) {
            Vector<Integer> weighting = vectors[v];
            fp.setFlipVector(weighting);
            String brain = fp.write();
            FutureTask<WeightScore> vecRunner = new FutureTask<WeightScore>(new ConcurrentRunner(brain, weighting, maps, wRandom, roundsMin, roundsMax, stepAmount));
            current.add(vecRunner);
            if (current.size() > 9 || (vectors.length - 1) == v) {
                List<Thread> threads = new ArrayList<>();
                for (FutureTask<WeightScore> currentRunner : current) {
                    Thread thread = new Thread(currentRunner);
                    thread.start();
                    updateProgressStart(j, max);
                    j++;
                    threads.add(thread);
                }
                int threadAmount = threads.size();
                int c = 1;
                for (Thread thread : threads) {
                    try {
                        thread.join();
                        System.out.print("  THREADS JOINED:" + (c) + '/' + threadAmount + '\n');
                        c++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                for (FutureTask<WeightScore> task : current) {
                    WeightScore score = null;
                    try {
                        score = task.get();
                        scoring.add(score);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    System.out.print("      Weights " + getdisplaystring(score.getWeights()) + " reached " + score.getScore() + " Points\n");
                }
                Collections.sort(scoring);
//                writeData(scoring.subList(0, 10));
//                Application.launch(ProgressDisplayer.class);
                current = new ArrayList<>();
            }
            runners.add(vecRunner);
        }

//        int i = 1;
//        for (FutureTask<WeightScore> runner : runners) {
//            try {
//                scoring.add(runner.get());
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//            updateProgress(i, max);
//            i++;
//        }

    }

    private void writeData(List<WeightScore> list) {
        try {
            System.out.print(new File("./").getAbsolutePath());
            FileWriter fileWriter = new FileWriter(new File("./data.txt"));
            for (WeightScore weightScore : list) {
                fileWriter.write(getDataString(weightScore));
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getDataString(WeightScore weightScore) {
        StringBuilder str = new StringBuilder();
        for (Integer integer : weightScore.getWeights()) {
            str.append(integer).append(',');
        }
        return str.append(':').append(weightScore.getScore()).append('\n').toString();
    }

    private void updateProgressStart(int j, int max) {
        System.out.print(">Currently Training:" + (j) + '/' + max + '\n');
    }

    private void updateProgress(int i, int max) {
        System.out.print(">Currently collected Scoring:" + (i) + '/' + max + '\n');
    }

    private String getdisplaystring(Vector<Integer> vec) {
        StringBuilder str = new StringBuilder().append('[');
        for (Iterator<Integer> iter = vec.iterator(); iter.hasNext(); ) {
            str.append(iter.next() + (iter.hasNext() ? ", " : ""));
        }
        return str.append(']').toString();
    }


    private Vector<Integer> getRandomVector(int length) {
        Vector<Integer> vec = new Vector<>();
        for (int i = 0; i < length; i++) {
            vec.add(wRandom.roll(this.flipMax));
        }
        return vec;
    }

}
